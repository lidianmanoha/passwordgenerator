const fs = require("fs");
const path = require("path");
const os = require("os");
const chalk = require("chalk");

const savePassword = (password) => {
	// full_path = os.path.realpath(__file__);
	console.log(process.cwd());
	fs.open(path.join(process.cwd(), "passwords.txt"), "a", (e, id) => {
		fs.write(id, password + os.EOL, null, "utf-8", () => {
			fs.close(id, () => {
				console.log(chalk.green("Password saved to passwords.txt"));
			});
		});
	});
};

module.exports = savePassword;
