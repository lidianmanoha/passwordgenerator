Create new password 
CMD -> node index.js OR passgen

Create new password with length choise size
CMD -> passgen --length=20 

Create new password with length choise size and no number
CMD -> passgen --length=20 --no-numbers

Create new password with length choise size and no number and not symbols
CMD -> passgen --length=20 --no-numbers -ns

Create new password and save to clipboard
CMD -> passgen --length=20 --save


HELP -> -h